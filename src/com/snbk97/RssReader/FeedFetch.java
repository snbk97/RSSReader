package com.snbk97.RssReader;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by sayan on 13-06-2017.
 *
 * More or less an HTTP client for RSS Reader.
 */
public class FeedFetch {
    private static OkHttpClient client = new OkHttpClient();
    private String url;

    FeedFetch(String url){
        this.url = url;
    }

    public String fetch() throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
