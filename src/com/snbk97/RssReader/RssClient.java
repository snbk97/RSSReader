package com.snbk97.RssReader;

import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by sayan on 15-06-2017.
 */
public class RssClient {
    private String source;

    RssClient(String source){
        this.source = source;
    }

    public void fetchContent()throws IOException {
        RssProvider rssProvider = new RssProvider(source);
        HashMap<Integer,JSONObject> nMap = rssProvider.getContent();

        Iterator<Integer> nMapIterator = nMap.keySet().iterator();
        while (nMapIterator.hasNext()){
            int key = nMapIterator.next();
            //System.out.println(nMap.get(key));
            new RssObject(nMap.get(key));
        }
    }
}
