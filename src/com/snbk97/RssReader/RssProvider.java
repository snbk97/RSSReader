package com.snbk97.RssReader;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by sayan on 14-06-2017.
 */
public class RssProvider {
    private static String apiRSS2JSON = "http://api.rss2json.com/v1/api.json?rss_url=";
    private static String source;


    RssProvider(String URL) {
        source = URL;
    }

    private enum RSS {
        HACKERNEWS ("https://news.ycombinator.com/rss"),
        THEVERGE  ("http://www.theverge.com/rss/index.xml"),
        BBC  ("http://feeds.bbci.co.uk/news/world/rss.xml"),
        THEGUARDIAN  ("https://www.theguardian.com/international/rss"),
        REDDIT ("http://www.reddit.com/.rss"),/*frontpage*/
        TOI ("http://timesofindia.indiatimes.com/rssfeedstopstories.cms");

        public final String URL;
        RSS(String s) {URL = s;}
    }

    private static String getURL(){
       return RSS.valueOf(source).URL;
    }

    private static String RSS2JSON() throws IOException{
        String $URL = getURL();
        apiRSS2JSON += $URL;
        return new FeedFetch(apiRSS2JSON).fetch();
    }

    public JSONArray parseJson() throws IOException {
        String JSON =  RSS2JSON();
        JSONParser parser = new JSONParser();
        JSONArray itemList = null;
        try {
            Object obj = parser.parse(JSON);
            JSONObject jsonObject = (JSONObject) obj;
            itemList = (JSONArray) jsonObject.get("items");
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return itemList;
    }

    public HashMap<Integer, JSONObject> getContent() throws IOException {
        int index = 0;
        HashMap<Integer,JSONObject> map= new HashMap<>();


        JSONArray itemsJSONArray = parseJson();
        for(Object X : itemsJSONArray){
            Object currentObject = X;
            JSONObject currentJSONObject = (JSONObject) currentObject;
            map.put(index,currentJSONObject);
            index++;
        }
       return map;
    }




}