package com.snbk97.RssReader;

import org.json.simple.JSONObject;

/**
 * Created by sayan on 15-06-2017.
 */
public class RssObject {
    public String thumbnail=null, author, link, description, title, pubDate, content;
    private static JSONObject nJSONObject;

    RssObject(JSONObject j){
        nJSONObject = j;
        extract();
        getInfo();
    }

    private void extract() {
        thumbnail = (String) nJSONObject.get("thumbnail");
        author = (String) nJSONObject.get("author");
        link = (String) nJSONObject.get("link");
        description = (String) nJSONObject.get("description");
        title = (String) nJSONObject.get("title");
        pubDate = (String) nJSONObject.get("pubDate");
        content = (String) nJSONObject.get("content");
    }
    public void getInfo(){
        /*if(thumbnail != " "){
            System.out.println("thumbnail: " + thumbnail);
        }*/
        System.out.println("link: " + link);
        System.out.println("title: " + title);
        System.out.println("pubDate: " + pubDate.split(" ")[0]);
        System.out.println("description: " + description);
        System.out.println();
    }
}
